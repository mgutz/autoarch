## Preseed file

1.  Press Enter when server install asks for language
2.  Press F6
3.  Edit boot params, replacing `file=*` attribute with:

    `auto=true priority=critical url=http://192.168.1.6:8000/preseed.cfg`

    priority prevents the installer from asking dumb questions like hostname
