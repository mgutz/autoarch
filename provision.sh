#!/bin/bash

# Override by prefixing script: NEW_USER=foo bash provision.sh
NEW_USER=${NEW_USER:-"coder"}
PASSWORD=${PASSWORD:-"password"}
HOSTNAME=${HOSTNAME:-"archbox"}

# Whether to install open-vm-tools
USE_VMWARE=${USE_VMWARE:-1}

PACMAN="pacman -S --noconfirm"

function announce {
	>&2 echo -n "$1"
}

function check_fail {
	if [[ $1 -ne 0 ]]; then
		>&2 echo "FAIL!"
		exit 1
	else
		>&2 echo "OK"
	fi
}


annountce "Upgrading... "
pcman -Syu --noconfirm

announce "Installing X... "
$PACMAN xorg-server xorg-server-utils xorg-xinit xorg-apps
check_fail $?

announce "Installing sound... "
$PACMAN alsa-utils pavucontrol pulseaudio-alsa
check_fail $?

announce "Installing XFCE... "
$PACMAN xfce4 xfce4-goodies gvfs ttf-liberation xdg-user-dirs opendesktop-fonts firefox
check_fail $?

announce "Installing LightDM sign-on... "
$PACMAN lightdm lightdm-gtk-greeter xorg-server-xephyr accountsservice \
	&& systemctl enable lightdm.service
check_fail $?

announce "Creating ${NEW_USER}'s account... "
useradd -m -g users -G video,audio,network,optical,storage,disk,wheel -s /bin/bash $NEW_USER
check_fail $?

announce "Setting ${NEW_USER}'s password... "
echo "$NEW_USER:$PASSWORD" | chpasswd
check_fail $?

announce "Setting hostname... "
hostnamectl set-hostname $HOSTNAME
check_fail $?

announce "Setting timezone... "
timedatectl set-timezone America/Los_Angeles
check_fail $?

if [[ $USE_VMWARE -eq 1 ]]; then
	announce "Installing vmware tools..."
	$PACMAN xf86-input-vmmouse xf86-video-vmware mesa open-vm-tools \
		&& systemctl enable vmtoolsd.service
	check_fail $?
fi

# announce "Allow users in wheel to sudo... "
# sed -i 's/^#en_US\.UTF/en_US\.UTF/' /mnt/etc/locale.gen \
